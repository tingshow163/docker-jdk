[![](https://images.microbadger.com/badges/image/tingshow163/docker-jdk.svg)](https://microbadger.com/images/tingshow163/docker-jdk "Get your own image badge on microbadger.com")
[![](https://images.microbadger.com/badges/version/tingshow163/docker-jdk.svg)](https://microbadger.com/images/tingshow163/docker-jdk "Get your own version badge on microbadger.com")

## 说明

使用 `alpine:3.9` 作为基础镜像，jdk 版本为 8u211。


## Changelog
* 2019-5-22: dump jdk version to 8u211
